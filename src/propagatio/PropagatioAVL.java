package propagatio;

import java.awt.*;
import java.util.Random;

/**
 * Created by liot on 21-01-2015.
 */
public class PropagatioAVL implements Propagatio<PropagatioAVL> {
    private int dato, balanceo;
    private static boolean propagacion;
    private PropagatioAVL izq, der;
    private static int Nivel;
    boolean isInfected = false;
    public Types type;

    public PropagatioAVL(int d) {
        dato = d;
        propagacion = true;
        balanceo = 0;
        izq = der = null;
    }


    @Override
    public PropagatioAVL insertar(PropagatioAVL t, int d) {
        if (t == null) return new PropagatioAVL(d);
        if (d == t.dato) return t;
        if (d < t.dato) {
            t.izq = insertar(t.izq, d);
            if (propagacion) {
                t.balanceo--;
                if (t.balanceo == -2) {
                    if (t.izq.balanceo < 0)
                        t = LL(t);
                    else
                        t = LR(t);
                    propagacion = false;
                    t = corrigeBalance(t);
                } else if (t.balanceo == 0)
                    propagacion = false;
            }
        } else {
            t.der = insertar(t.der, d);
            if (propagacion) {
                t.balanceo++;
                if (t.balanceo == 2) {
                    if (t.der.balanceo < 0)
                        t = RL(t);
                    else
                        t = RR(t);
                    propagacion = false;
                    t = corrigeBalance(t);
                } else if (t.balanceo == 0)
                    propagacion = false;
            }
        }
        return t;
    }

    public PropagatioAVL LL(PropagatioAVL t) {
        PropagatioAVL p = t.izq;
        try {
            t.izq = p.der;
            p.der = t;
        } catch (NullPointerException e) {
            System.err.println("NullPointerException: " + e.getMessage());
        }
        return p;
    }

    public PropagatioAVL RR(PropagatioAVL t) {
        PropagatioAVL p = t.der;
        try {
            t.der = p.izq;
            p.izq = t;
        } catch (NullPointerException e) {
            System.err.println("NullPointerException: " + e.getMessage());
        }
        return p;
    }

    public PropagatioAVL LR(PropagatioAVL t) {
        PropagatioAVL p = t.izq;
        PropagatioAVL h = p.der;
        try {
            t.izq = h.der;
            p.der = h.izq;
            h.der = t;
            h.izq = p;
        } catch (NullPointerException e) {
            System.err.println("NullPointerException: " + e.getMessage());
        }
        return h;

    }

    public PropagatioAVL RL(PropagatioAVL t) {

        PropagatioAVL p = t.der;
        PropagatioAVL h = p.izq;
        try {
            t.der = h.izq;
            p.izq = h.der;
            h.izq = t;
            h.der = p;
        } catch (NullPointerException e) {
            System.err.println("NullPointerException: " + e.getMessage());
        }
        return h;
    }

    public PropagatioAVL corrigeBalance(PropagatioAVL t) {
        if (t != null) {
            Nivel = 0;
            numeroDeNiveles(t.izq, 1);
            int a = Nivel;
            Nivel = 0;
            numeroDeNiveles(t.der, 1);
            int b = Nivel;
            t.balanceo = b - a;
            corrigeBalance(t.izq);
            corrigeBalance(t.der);
        }
        return t;
    }

    private void numeroDeNiveles(PropagatioAVL t, int nivel) {
        if (t != null) {
            if (nivel > Nivel)
                Nivel++;
            numeroDeNiveles(t.izq, nivel + 1);
            numeroDeNiveles(t.der, nivel + 1);
        }

    }

    @Override
    public PropagatioAVL eliminar(PropagatioAVL t, int x) {
        if (t == null)
            return null;
        if (x == t.dato)
            if (t.izq == null)
                return t.der;
            else if (t.izq.der == null) {
                t.dato = t.izq.dato;
                t.izq = t.izq.izq;

                return t;
            } else {
                PropagatioAVL p, h = null;
                p = t.izq;
                h = h.der;
                while (h.der != null) {
                    p = h;
                    h = h.der;
                }
                t.dato = h.dato;
                p.der = h.izq;
                return t;
            }
        else if (x < t.dato)
            t.izq = eliminar(t.izq, x);
        else
            t.der = eliminar(t.der, x);
        return t;
    }

    @Override
    public PropagatioAVL eliminar(int x) {
        PropagatioAVL t = this;
        t = t.eliminar(t, x);
        t = corrigeBalance(t);
        t = recorreYRotaDeSerNecesario(t);
        return t;

    }

    private PropagatioAVL recorreYRotaDeSerNecesario(PropagatioAVL t) {
        if (t != null) {
            if (t.balanceo == -2) {
                if (t.izq.balanceo < 0)
                    t = LL(t);
                else
                    t = LR(t);
                t = corrigeBalance(t);
            }
            if (t.balanceo == 2) {
                if (t.der.balanceo < 0)
                    t = RL(t);
                else
                    t = RR(t);
                t = corrigeBalance(t);
            }
            t.izq = recorreYRotaDeSerNecesario(t.izq);
            t.der = recorreYRotaDeSerNecesario(t.der);


        }
        return t;
    }

    @Override
    public void dibujame(Graphics g, PropagatioAVL prop, int i, int j,
                         int pi, int pj, int desp, boolean fatherIsInfected) {
        if (prop != null) {
            boolean isInfected = false;
            if (prop.type == null)
                prop = setType(prop);
            if (fatherIsInfected)
                prop = propagates(prop);
            if (prop.type == Types.SICK)
                isInfected = true;

            dibujame(g, prop.izq, i - desp / 2, j + 60, i, j, desp / 2,
                    isInfected);
            dibujame(g, prop.der, i + desp / 2, j + 60, i, j, desp / 2,
                    isInfected);

            g.setColor(new Color(200, 200, 200));
            g.drawLine(i + 15, j + 15, pi + 15, pj + 15);

            switch (prop.type.getLabel()) {
                case "ROOT":
                    g.setColor(new Color(58, 22, 22));
                    break;
                case "NODE":
                    g.setColor(new Color(43, 59, 176));
                    break;
                case "LEAF":
                    g.setColor(new Color(11, 208, 44));
                    break;
                case "SICK":
                    g.setColor(new Color(208, 44, 11));
                    break;
            }

            //g.drawString(String.valueOf(prop.dato), i + 3, j + 20);
            g.fillOval(i, j, 30, 30);


        }
    }

    @Override
    public PropagatioAVL setType(PropagatioAVL prop) {
        if (prop != null) {
            if (prop.izq == null && prop.der == null)
                prop.type = Types.LEAF;
            else
                prop.type = Types.NODE;
        }
        return prop;
    }

    @Override
    public void setType(Types type) {
        this.type = type;
    }

    @Override
    public int getDepth(PropagatioAVL prop) {

        int a, b;
        a = b = 0;
        if (prop != null) {
            if (prop.izq == null && prop.der == null)
                return 1;
            a += getDepth(prop.izq);
            b += getDepth(prop.der);
            if (a < b)
                a = b;
        }
        return a;

    }

    @Override
    public PropagatioAVL propagates(PropagatioAVL prop) {
        if (prop != null) {
            Integer prob = null;
            Random r = new Random();
            for (int i = 0; i < 100; i++)
                prob = r.nextInt(2);
            if (prob == 1) {
                prop.type = Types.SICK;
            }
        }
        return prop;
    }

    @Override
    public void preOrden(PropagatioAVL t) {
        if (t != null) {
            System.out.print("  " + t.dato);
            preOrden(t.izq);
            preOrden(t.der);
        }
    }

    @Override
    public void inOrden(PropagatioAVL t) {
        if (t != null) {
            inOrden(t.izq);
            System.out.print("  " + t.dato);
            inOrden(t.der);
        }
    }

    @Override
    public void postOrden(PropagatioAVL t) {
        if (t != null) {
            postOrden(t.izq);
            postOrden(t.der);
            System.out.print("  " + t.dato);

        }
    }

    @Override
    public boolean isInfected() {
        return isInfected;
    }

    @Override
    public void isInfected(boolean isInfected) {
        this.isInfected = isInfected;
    }


}
