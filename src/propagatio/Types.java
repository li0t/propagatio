package propagatio;

/**
 * Created by liot on 21-01-2015.
 */
public enum Types {
    ROOT(1, "ROOT"),
    NODE(2, "NODE"),
    LEAF(3, "LEAF"),
    SICK(4, "SICK");

    private final int id;
    private final String label;

    private Types(int id, String label) {
        this.id = id;
        this.label = label;
    }

    public int getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public static Types getEnum(Long id) {
        if (id != null) {
            for (Types e : Types.values()) {
                if (e.getId() == id.intValue()) {
                    return e;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return label;
    }

}
