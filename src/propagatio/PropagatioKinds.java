package propagatio;

/**
 * Created by liot on 21-01-2015.
 */
public enum PropagatioKinds {
    PROPAGATIO_BINARY(1, "PROPAGATIO_BINARY"),
    PROPAGATIO_AVL(2, "PROPAGATIO_AVL");


    private final int id;
    private final String label;

    private PropagatioKinds(int id, String label) {
        this.id = id;
        this.label = label;
    }

    public int getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public static PropagatioKinds getEnum(Long id) {
        if (id != null) {
            for (PropagatioKinds e : PropagatioKinds.values()) {
                if (e.getId() == id.intValue()) {
                    return e;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return label;
    }

}
