package propagatio;

import java.util.ArrayList;

/**
 * Created by liot on 21-01-2015.
 */

public class Medidor {
    protected ArrayList<Integer> datos;
    protected ArrayList<ArrayList<Integer>> datos2d;
    protected Integer B, A;

    public Medidor() {
        datos = new ArrayList<Integer>();
        datos2d = new ArrayList<ArrayList<Integer>>();
        B = A = null;

    }

    public void addData(int dato) {
        datos.add(dato);
    }

    public void addData2d(int x, int y) {
        datos2d.get(0).add(x);
        datos2d.get(1).add(y);

    }

    @SuppressWarnings("null")
    public int media() {
        Integer media = null;

        for (int dato : datos)
            media += dato;

        return media / datos.size();
    }

    @SuppressWarnings("null")
    public int media(ArrayList<Integer> datos) {
        Integer media = null;

        for (int dato : datos)
            media += dato;

        return media / datos.size();
    }

    @SuppressWarnings("null")
    public int varianza() {
        int media = media();

        Integer varianza = null;

        for (int dato : datos)
            varianza += (int) (Math.pow(dato - media, 2));

        return varianza / (datos.size() - 1);
    }

    @SuppressWarnings("null")
    public int varianza(int media, ArrayList<Integer> datos) {

        Integer varianza = null;

        for (int dato : datos)
            varianza += (int) (Math.pow(dato - media, 2));

        return varianza / (datos.size() - 1);
    }

    public int desviacionEstandar() {

        return (int) (Math.sqrt(varianza()));

    }

    public int desviacionEstandar(int varianza) {

        return (int) (Math.sqrt(varianza));

    }

    public int variabilidad() {
        return (desviacionEstandar() / media() * 100);
    }

    @SuppressWarnings("null")
    public int mediaConjunta() {
        Integer mediaConjunta = null;

        for (int i = 0; i < datos2d.get(0).size(); i++) {
            mediaConjunta += datos2d.get(0).get(i) * datos2d.get(1).get(i);
        }

        return mediaConjunta / datos2d.get(0).size();
    }

    public int covarianza() {
        int mediaX = media(datos2d.get(0));
        int mediaY = media(datos2d.get(1));

        return mediaConjunta() - (mediaX * mediaY);
    }

    public int correlacion() {

        int varianzaX = varianza(media(datos2d.get(0)), datos2d.get(0));
        int varianzaY = varianza(media(datos2d.get(1)), datos2d.get(1));

        int desvEstX = desviacionEstandar(varianzaX);
        int desvEstY = desviacionEstandar(varianzaY);

        return covarianza() / (desvEstX * desvEstY);
    }

    public int bondad() {
        return (int) (Math.pow(correlacion(), 2));
    }

    protected void B() {
        int varianzaX = varianza(media(datos2d.get(0)), datos2d.get(0));

        int desvEstX = desviacionEstandar(varianzaX);

        this.B = covarianza() / desvEstX;
        A();
    }

    protected void A() {
        int mediaX = media(datos2d.get(0));
        int mediaY = media(datos2d.get(1));

        if (this.B == null)
            B();
        this.A = mediaY - (this.B * mediaX);

    }

    public int regresionLineal(int x) {
        if(this.A == null)
            B();

        return (this.A + this.B * x);

    }
}
