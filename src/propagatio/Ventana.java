package propagatio;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.HashMap;



/**
 * Created by liot on 21-01-2015.
 */

public class Ventana extends Frame implements ActionListener, WindowListener {

    HashMap<PropagatioKinds, Propagatio> propKinds;
    PropagatioKinds kind;
    Propagatio prop;
    Button btnReset, btnABB, btnAVL;
    SpinnerNumberModel numberModel;
    Integer rango, nNodos,min, max, step;

    private void setKinds(int rango) {
        propKinds = new HashMap<>();
        propKinds.put(PropagatioKinds.PROPAGATIO_BINARY, new PropagatioBinary(rango / 2));
        propKinds.put(PropagatioKinds.PROPAGATIO_AVL, new PropagatioAVL(rango / 2));
    }

    Ventana(int r) {
        rango = r;

        setSize(1200, 800);
        setLocation(150, 34);
        setBackground(new Color(132, 46, 156));
        setLayout(null);
        setResizable(false);

        nNodos = 25;

        /**
        *Necesito implementar JPanel
        */
        /*min = 0;
        max = 100;
        step = 1;
        numberModel = new SpinnerNumberModel(nNodos, min, max, step);
        JSpinner nmbrSpinner = new JSpinner(numberModel);
        nmbrSpinner.setBounds(100, 750, 60, 30);
        add(nmbrSpinner);*/

        setKinds(rango);
        kind = PropagatioKinds.PROPAGATIO_BINARY;
        prop = propKinds.get(kind);

        btnReset = new Button("RESET");
        btnReset.setBounds(1100, 750, 60, 30);
        add(btnReset);

        btnABB = new Button("ABB");
        btnABB.setBounds(540, 750, 60, 30);
        add(btnABB);

        btnAVL = new Button("AVL");
        btnAVL.setBounds(640, 750, 60, 30);
        add(btnAVL);

        btnReset.addActionListener(this);
        btnABB.addActionListener(this);
        btnAVL.addActionListener(this);
        addWindowListener(this);
    }

    public void paint(Graphics g) {
        if (prop != null)
            prop.dibujame(g, prop, 600, 40, 600, 40, 450, prop.isInfected());
    }


    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        int cantidadNodos = nNodos;
        setKinds(rango);
        if (actionEvent.getSource() == btnABB)
            kind = PropagatioKinds.PROPAGATIO_BINARY;
        if (actionEvent.getSource() == btnAVL)
            kind = PropagatioKinds.PROPAGATIO_AVL;
        prop = propKinds.get(kind);
        prop.isInfected(true);
        if (actionEvent.getSource() == btnReset) {
            cantidadNodos = 0;
            prop.isInfected(false);
        }
        java.util.Random r = new java.util.Random();
        for (int i = 0; i < cantidadNodos; i++)
            prop = prop.insertar(prop, r.nextInt(rango));
        this.repaint();
    }


    @Override
    public void windowClosing(WindowEvent windowEvent) {
        dispose();
        System.exit(0);
    }

    @Override
    public void windowOpened(WindowEvent windowEvent) {

    }

    @Override
    public void windowClosed(WindowEvent windowEvent) {

    }

    @Override
    public void windowIconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeiconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowActivated(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeactivated(WindowEvent windowEvent) {

    }
}
