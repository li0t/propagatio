package propagatio;

/**
 * Created by liot on 21-01-2015.
 */

        import java.awt.Color;
        import java.awt.Graphics;
        import java.util.Random;

public class PropagatioBinary implements Propagatio<PropagatioBinary> {
    protected PropagatioBinary izq, der;
    protected Types type;
    private int dato;
    boolean isInfected = false;

    public PropagatioBinary(int d) {
        dato = d;
        izq = der = null;
    }

    @Override
    public PropagatioBinary insertar(PropagatioBinary prop, int d) {
        if (prop == null)
            return new PropagatioBinary(d);
        if (d < prop.dato)
            prop.izq = insertar(prop.izq, d);
        else
            prop.der = insertar(prop.der, d);
        return prop;
    }

    @Override
    public PropagatioBinary eliminar(PropagatioBinary prop, int x) {
        return null;
    }

    @Override
    public PropagatioBinary eliminar(int x) {
        return null;
    }

    @Override
    public void dibujame(Graphics g, PropagatioBinary prop, int i, int j,
                         int pi, int pj, int desp, boolean fatherIsInfected) {
        if (prop != null) {
            boolean isInfected = false;
            if (prop.type == null)
                prop = setType(prop);
            if (fatherIsInfected)
                prop = propagates(prop);
            if (prop.type == Types.SICK)
                isInfected = true;

            dibujame(g, prop.izq, i - desp / 2, j + 60, i, j, desp / 2,
                    isInfected);
            dibujame(g, prop.der, i + desp / 2, j + 60, i, j, desp / 2,
                    isInfected);

            g.setColor(new Color(200, 200, 200));
            g.drawLine(i + 15, j + 15, pi + 15, pj + 15);

            switch (prop.type.getLabel()) {
                case "ROOT":
                    g.setColor(new Color(58, 22, 22));
                    break;
                case "NODE":
                    g.setColor(new Color(43, 59, 176));
                    break;
                case "LEAF":
                    g.setColor(new Color(11, 208, 44));
                    break;
                case "SICK":
                    g.setColor(new Color(208, 44, 11));
                    break;
            }

            //g.drawString(String.valueOf(prop.dato), i + 3, j + 20);
            g.fillOval(i, j, 30, 30);


        }
    }


    @Override
    public boolean isInfected() {
        return isInfected;
    }

    @Override
    public void isInfected(boolean isInfected) {
        this.isInfected = isInfected;
    }

    @Override
    public PropagatioBinary setType(PropagatioBinary prop) {
        if (prop != null) {
            if (prop.izq == null && prop.der == null)
                prop.type = Types.LEAF;
            else
                prop.type = Types.NODE;
        }
        return prop;
    }

    public void setType(Types type) {
        this.type = type;
    }

    @Override
    public int getDepth(PropagatioBinary prop) {

        int a, b;
        a = b = 0;
        if (prop != null) {
            if (prop.izq == null && prop.der == null)
                return 1;
            a += getDepth(prop.izq);
            b += getDepth(prop.der);
            if (a < b)
                a = b;
        }
        return a;

    }

    @Override
    public PropagatioBinary propagates(PropagatioBinary prop) {
        if (prop != null) {
            Integer prob = null;
            Random r = new Random();
            for (int i = 0; i < 100; i++)
                prob = r.nextInt(2);
            if (prob == 1) {
                prop.type = Types.SICK;
            }
        }
        return prop;
    }

    @Override
    public void preOrden(PropagatioBinary t) {
        if (t != null) {
            System.out.print("  " + t.dato);
            preOrden(t.izq);
            preOrden(t.der);
        }
    }

    @Override
    public void inOrden(PropagatioBinary t) {
        if (t != null) {
            inOrden(t.izq);
            System.out.print("  " + t.dato);
            inOrden(t.der);
        }
    }

    @Override
    public void postOrden(PropagatioBinary t) {
        if (t != null) {
            postOrden(t.izq);
            postOrden(t.der);
            System.out.print("  " + t.dato);

        }
    }

}
