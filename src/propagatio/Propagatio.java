package propagatio;

import java.awt.*;


/**
 * Created by liot on 21-01-2015.
 */
public interface Propagatio<P extends Propagatio<P>> {

    P insertar(P prop, int d);

    P eliminar(P prop, int x);

    P eliminar(int x);

    void dibujame(Graphics g, P prop, int i, int j,
                  int pi, int pj, int desp, boolean fatherIsInfected);

    P setType(P prop);

    void setType(Types type);

    int getDepth(P prop);

    P propagates(P prop);

    void preOrden(P prop);

    void inOrden(P prop);

    void postOrden(P prop);

    boolean isInfected();

    void isInfected(boolean isInfected);

}
